class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.datetime :completed_at
      t.decimal  :amount, :precision => 8, :scale => 2

      t.timestamps null: false
    end
  end
end

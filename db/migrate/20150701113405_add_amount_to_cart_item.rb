class AddAmountToCartItem < ActiveRecord::Migration
  def change
    add_column :cart_items, :amount, :decimal, :precision => 8, :scale => 2
  end
end

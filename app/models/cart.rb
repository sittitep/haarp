class Cart < ActiveRecord::Base
	has_many :cart_items
	accepts_nested_attributes_for :cart_items, :allow_destroy => true

	def add_cart_item(params)
		if (cart_item = cart_items.find_by_product_id params[:product_id])
			cart_item.quantity += params[:quantity].to_i
			cart_item.save
		else
			cart_items.create(params)
		end
	end

	def update!
		cart_items.each(&:update!)
		update_attributes(amount: calculate_amount)
	end

	def calculate_amount
		cart_items.sum("amount")
	end

	def get_size
		cart_items.sum(:quantity)
	end
end

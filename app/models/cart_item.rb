class CartItem < ActiveRecord::Base
	belongs_to :product
	belongs_to :cart

	def update!
		update_attributes(amount: calculate_amount)
	end

	def calculate_amount
		product.price * quantity
	end
end

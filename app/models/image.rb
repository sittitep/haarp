class Image < ActiveRecord::Base
	has_attached_file :file, :styles => {:thumb => 'x100', :croppable => '600x600>', :big => '1000x1000>'}
	validates_attachment :file, :presence => true,
  :content_type => { :content_type => "image/jpeg" }
	belongs_to :imageable, polymorphic: true
end

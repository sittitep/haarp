class Product < ActiveRecord::Base
	acts_as_taggable
	extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :cart_items
  has_many :images, as: :imageable
  accepts_nested_attributes_for :images, :allow_destroy => true

  def first_image(size = :original)
  	images.first.file.url(size) if images.present?
  end
end

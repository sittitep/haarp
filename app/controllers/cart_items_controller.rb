class CartItemsController < ApplicationController
	def create
		current_cart.add_cart_item(cart_items_params)
		current_cart.update!
	end

	private
		def cart_items_params
			params.require(:cart_items).permit!
		end
end

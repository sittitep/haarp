class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_cart

  def current_cart
  	if cookies[:current_cart_id].present?
  		cart = Cart.find cookies[:current_cart_id].to_i
  		if cart.completed_at
  			cart = Cart.create
  			cookies[:current_cart_id] = cart.id
  		end	
  	else
  		cart = Cart.create
  		cookies[:current_cart_id] = cart.id
  	end

  	return cart
  end
end
